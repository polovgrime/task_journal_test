﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace task_journal.Models
{
    public class Place
    {
        public string Address { get;set;}
        public string Name { get;set;}
        public int Id { get;set;}
    }
}
