﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Threading.Tasks;

namespace task_journal.Models.ViewModels
{
    public class NewTaskModel
    {
        private const int text_max_length = 3000;
        private const int theme_max_length = 150;
        public string Place { get; set; }
        [Required(ErrorMessage = "Отсутствует содержание заявки")]

        private string text;
        public string Text {
            get { return text; }
            set {
                text = value;
                if (text.Length > text_max_length)
                {
                    text = text.Substring(0, text_max_length);
                }
            }
        }

        //   [Range(0, 150)]
        private string theme;
        [Required(ErrorMessage = "Не указана тема")]
        public string Theme {
            get { return theme; }
            set {
                theme = value;
                if (theme.Length > theme_max_length)
                    theme = theme.Substring(0, theme_max_length);
            }
        }
        public string Executor { get; set; }
    }
}
