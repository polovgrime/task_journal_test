﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Threading.Tasks;

namespace task_journal.Models.ViewModels
{
    public class RegisterModel
    {
        [Required(ErrorMessage = "Не указан e-mail")]

        public string Email { get; set; }
        [Required(ErrorMessage = "Не указан логин")]

        public string Login { get; set; }

        [Required(ErrorMessage = "Не указан пароль")]
        [DataType(DataType.Password)]
        public string Password { get;set;}
    }
}
