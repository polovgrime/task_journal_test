﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace task_journal.Models
{

    public enum SortStyle
    {
        ByDate, ById, ByExecutor, ByAuthor, ByTheme, ByCondition, ByPlace
    }

    public enum SortDirection
    {
        Up, Down
    }

    public struct SortToken
    {
        public SortStyle sorting;
        public SortDirection direction;
        public SortToken(SortStyle sorting, SortDirection direction)
        {
            this.sorting = sorting;
            this.direction = direction;
        }
    }
    public class Sorter
    {
        IEnumerable<Target> table;
        SortToken token;
        public Sorter(IEnumerable<Target> tempTable, SortToken tempToken)
        {
            table = tempTable;
            token = tempToken;
        }
        public IEnumerable<Target> GetSortedTable()
        {
            switch (token.sorting)
            {

                case SortStyle.ByPlace:
                    SortPlace();
                break;
                case SortStyle.ByDate:
                    SortDate();
                break;
                case SortStyle.ByAuthor:
                    SortAuthor();
                break;
                case SortStyle.ByExecutor:
                    SortExecutor();
                break;
                case SortStyle.ByTheme:
                    SortTheme();
                break;
                case SortStyle.ById:
                    SortId();
                break;
                case SortStyle.ByCondition:
                    SortCondition();
                break;
            }
            return table;
        }

        private void SortPlace()
        {
            if (token.direction == SortDirection.Up)
                table = table.OrderBy(element => element.Place);
            else
                table = table.OrderByDescending(element => element.Place);
        }
        
        private void SortCondition()
        {
            if (token.direction == SortDirection.Up)
                table = table.OrderBy(element => element.IsClosed);
            else
                table = table.OrderByDescending(element => element.IsClosed);
        }

        private void SortDate()
        {
            if (token.direction == SortDirection.Up)
                table = table.OrderBy(element => element.Date);
            else 
                table = table.OrderByDescending(elements => elements.Date);
        }

        private void SortAuthor()
        {
            if (token.direction == SortDirection.Up) 
                table = table.OrderBy(element => element.Author);
            else
                table = table.OrderByDescending(element => element.Author);
        }

        private void SortExecutor()
        {
            if (token.direction == SortDirection.Up)
                table = table.OrderBy(element => element.Executor);
            else
                table = table.OrderByDescending(element => element.Executor);
        }

        private void SortTheme()
        {
            if (token.direction == SortDirection.Up)
                table = table.OrderBy(element => element.Theme);
            else
                table = table.OrderByDescending(element => element.Theme);
        }

        private void SortId()
        {
            if (token.direction == SortDirection.Up)
                table = table.OrderBy(element => element.Id);
            else 
                table = table.OrderByDescending(element => element.Id);
        }

    }
}
