﻿
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Runtime.Serialization;
using System.Runtime.Serialization.Json;

namespace task_journal.Models
{
    public class Target
    {
        private bool isClosed;
        public int Id { get; set;}
        public string Place { get; set;}
        public string Theme { get; set;}
 //       [Range(0, 3000)]
        public string Text { get; set;}
        public string Executor { get; set;}
        private int ExecutorId { get;set;}
        public string Author { get;set;}
        public DateTime Date { get;set;}
        public int IsClosed {
            get {
                if (isClosed)
                    return 1;
                else 
                    return 0;
            }
            set {
                if (value >= 1)
                    isClosed = true;
                else 
                    isClosed = false;
            }

        }
    }

    public struct TargetViewData
    {
        public Target Target { get;set;}
        public List<Executor> Executors { get;set;}
        public List<Place> Places { get;set;}
    }

    
    public class Comment
    {

    
        public int Id { get;set;}
    
        public string Author { get;set;}
    
        public DateTime Date { get;set;}
    
        public string Text { get;set;}

        public int TaskId { get;set; }
        public int IsHidden { get;set;}
    }

}
