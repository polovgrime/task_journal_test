﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace task_journal.Models
{
    public class Executor
    {
        public int Id { get; set;}
        public string Name { get; set;}
        public string Phone { get; set;}
    }
}
