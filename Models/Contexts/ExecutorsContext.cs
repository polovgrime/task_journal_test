﻿using Microsoft.EntityFrameworkCore;

namespace task_journal.Models.Contexts
{
    public class ExecutorsContext : DbContext
    {
        public DbSet<Executor> Executors { get; set; }
        public ExecutorsContext(DbContextOptions<ExecutorsContext> options) : base(options) { } 
    }
}
