﻿using Microsoft.EntityFrameworkCore;

namespace task_journal.Models.Contexts
{
    public class PlaceContext : DbContext
    {
        public DbSet<Place> Places { get; set;}
        public PlaceContext(DbContextOptions<PlaceContext> options) : base(options) { }
    }
}
