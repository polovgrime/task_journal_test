﻿using Microsoft.EntityFrameworkCore;

namespace task_journal.Models.Contexts
{
    public class TargetsContext : DbContext
    {
        public DbSet<Target> Tasks { get; set;}
        public TargetsContext(DbContextOptions<TargetsContext> options) : base(options)
        {

        }
    }
}
