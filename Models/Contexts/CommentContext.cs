﻿using Microsoft.EntityFrameworkCore;
namespace task_journal.Models.Contexts
{
    public class CommentContext : DbContext
    {
        public DbSet<Comment> Comments { get;set;}
        
        public CommentContext (DbContextOptions<CommentContext> options) : base(options) { }
    }
}
