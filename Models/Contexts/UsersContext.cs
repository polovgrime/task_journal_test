﻿using Microsoft.EntityFrameworkCore;

namespace task_journal.Models.Contexts
{
    public class UsersContext : DbContext
    {
        public DbSet<User> Users { get;set;}
        public UsersContext (DbContextOptions<UsersContext> options) : base(options) {
          //  Database.EnsureCreated();
        }
    }
}
