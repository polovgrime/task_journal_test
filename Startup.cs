﻿
using Microsoft.AspNetCore.Builder;
using Microsoft.AspNetCore.Hosting;
using Microsoft.Extensions.Configuration;
using Microsoft.Extensions.DependencyInjection;
using Microsoft.EntityFrameworkCore;
using Microsoft.AspNetCore.Authentication.Cookies;
using task_journal.Models.Contexts;
using Microsoft.AspNetCore.Http;
using Microsoft.Extensions.Logging;
using Microsoft.AspNetCore.Routing;
using System.Reflection.Metadata;

namespace task_journal
{
    public class Startup
    {
        public Startup(IConfiguration configuration)
        {
            Configuration = configuration;
            
        }

        public IConfiguration Configuration { get; }

        // This method gets called by the runtime. Use this method to add services to the container.
        public void ConfigureServices(IServiceCollection services)
        {

            services.AddMvc();
            services.AddRouting();
            string testbasecon = @"Data Source=ANTONSYSPC\SQLEXPRESS;Initial Catalog=testbase;Integrated Security=True";
          //  string testbasecon = @"Data Source=DESKTOP-VU0AIAM;Initial Catalog=testbase;Integrated Security=True";
            services.Configure<IISOptions>(options =>
            {
                options.AutomaticAuthentication = true;
            });

            services.AddDbContext<UsersContext>(options => options.UseSqlServer(testbasecon));
            services.AddDbContext<TargetsContext>(options => options.UseSqlServer(testbasecon));
            services.AddDbContext<ExecutorsContext>(options => options.UseSqlServer(testbasecon));
            services.AddDbContext<PlaceContext>(options => options.UseSqlServer(testbasecon));
            services.AddDbContext<CommentContext>(options => options.UseSqlServer(testbasecon));
            services.AddAuthentication(CookieAuthenticationDefaults.AuthenticationScheme)
                .AddCookie(options =>
                {
                    options.LoginPath = "/auth/login";
                    options.AccessDeniedPath = "/auth/accessdenied";
                    
                });

        }

        // This method gets called by the runtime. Use this method to configure the HTTP request pipeline.
        public void Configure(IApplicationBuilder app, IHostingEnvironment env, ILoggerFactory loggerFactory)
        {

            app.UseDeveloperExceptionPage();

            //    app.UseHttpsRedirection();
            app.UseStaticFiles();
            //      app.UseDefaultFiles();
            app.UseAuthentication();
            //     app.UseHttpsRedirection();
            app.UseMvc();
        }
    }
}
