﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.IO;
using System.Security.Claims;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using task_journal.Models;
using task_journal.Models.Contexts;
using task_journal.Models.ViewModels;

namespace task_journal.Controllers
{
    [Route("newtask")]

    public class NewTaskController : Controller
    {
        public IActionResult NewTarget()
        {
            ViewBag.Executors = executorsContext.Executors.ToList();
            ViewBag.Places = placeContext.Places.ToList();
            return View(new NewTaskModel());
        }
        const string commentBase = @"commentBase/";
        
        TargetsContext taskContext;
        ExecutorsContext executorsContext;
        PlaceContext placeContext;
        public NewTaskController(TargetsContext targetsContext, ExecutorsContext eContext, PlaceContext pContext)//, PlaceContext pContext)
        {
            placeContext = pContext;
            taskContext = targetsContext;
            executorsContext = eContext;
        }
        [HttpPost]
        public IActionResult AAAA(NewTaskModel model)
        {
            Target target = new Target {Theme = model.Theme ?? "пусто", Executor = model.Executor ?? "пусто", Place = model.Place ?? "пусто", Text = model.Place ?? "пусто" };
            if (target == null)
                return BadRequest("aaa");

//            target.Executor = executorsContext.Executors.FirstOrDefault(e => e.Id == executorId)?.Name ?? "";
            target.Author = User.Claims.First(e => e.Type == ClaimTypes.Name).Value; //Айди микрочелика
            if (target.Author == "" || target.Theme == "" || target.Executor == "")
                return BadRequest("aaaaa");

            target.Date = DateTime.Now;
            target.IsClosed = 0;
            try
            {
                taskContext.Add(target);
                taskContext.SaveChanges();
            }
            catch
            {
                return BadRequest();
            }
            return Redirect("alltasks");
        }
    }
}