﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;

namespace task_journal.Controllers
{
    
    public class HomeController : Controller
    {
        [Route("")]
        public IActionResult Index()
        {
            return Redirect("alltasks");
        }

        [Route("userinfo")]
        public IActionResult UserInfo()
        {
            return View();
        }

        [Route("api/values")]
        [HttpGet]
        public string GetUser()
        {
            if (!User.Identity.IsAuthenticated)
                return "папущен" + User.Identity.Name;
            return "Не авторизован";
        }
    }
}