﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Security.Claims;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Authentication;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using task_journal.Models;
using task_journal.Models.Contexts;
using task_journal.Models.ViewModels;

namespace TestAuth.Controllers
{
    [Route("auth")]
    public class AuthController : Controller
    {
       
        private readonly UsersContext userService;

        public AuthController(UsersContext userService)
        {
            this.userService = userService;
        }

        [Route("login")]
        public IActionResult LogIn()
        {
            if (User.Identity.IsAuthenticated)
                return RedirectToAction("Index", "Home");
            return View(new LoginModel());
        }



        [Route("login")]
        [ValidateAntiForgeryToken]
        [HttpPost]
        public async Task<IActionResult> LogIn(LoginModel model)
        {
            if (User.Identity.IsAuthenticated)
                return RedirectToAction("Index", "Home");
            if (!ModelState.IsValid)
            {
                return View(model);
            }

            List<User> users = userService.Users.ToList();
            var temp = users.First();
            while (model.Name.Length < temp.Name.Length)
            {
                model.Name = model.Name + " ";
            }
            while (model.Password.Length < temp.Password.Length)
            {
                model.Password = model.Password + " ";
            }
            var user = users.FirstOrDefault(e => e.Name == model.Name && e.Password == model.Password);
            if (user == null)
            {
                ModelState.AddModelError("InvalidCredentials", "Could not validate your credentials");
                return View(model);
            }

            return await SignInUser(user);
        } /*
        [Route("accessdenied")]
        public IActionResult AccessDenied()
        {
            return View();
        }
        */
        [Route("logout")]
        [ValidateAntiForgeryToken]
        [HttpPost]
        public async Task<IActionResult> LogOut()
        {
            await HttpContext.SignOutAsync();
            return RedirectToAction("Index", "Home");
        }
        private async Task<IActionResult> SignInUser(User user)
        {
            var claims = new List<Claim>
            {
                new Claim(ClaimTypes.Name, user.DisplayedName),
                new Claim(ClaimTypes.NameIdentifier, user.Name),
                new Claim(ClaimTypes.SerialNumber, user.Id.ToString()),
                new Claim(ClaimTypes.Role, user.Role ?? "")
            };
            var identity = new ClaimsIdentity(claims, "Custom");
            var principal = new ClaimsPrincipal(identity);

            await HttpContext.SignInAsync(principal);

            return RedirectToAction("AllTasks", "Targets");
        }
    }

}
