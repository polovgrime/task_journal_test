﻿
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using task_journal.Models;
using task_journal.Models.Contexts;
using System.IO;
using System.Runtime.Serialization.Json;
using Microsoft.AspNetCore.Authorization;

namespace task_journal.Controllers
{
    [Authorize]
    [Route("testsegment228")]
    [ApiController]
    public class TaskPageController : Controller
    {
        const string commentBase = @"commentBase/";
        const string commentFolder = @"taskCommentNo";
        
        TargetsContext taskContext;
        PlaceContext placeContext;
        CommentContext commentContext;
        public TaskPageController(PlaceContext pc, TargetsContext tc, CommentContext cc)
        {
            taskContext = tc;
            placeContext = pc;
            commentContext = cc;
        }
        [HttpGet]
        [Route("comments/{id}")]
        public List<Comment> GetComments(int id)
        {
            List<Comment> comments = commentContext.Comments.Where(task => task.Id == id).ToList();
            return comments;
        }
        [HttpPost]
        [Route("comments/{id}/new-commn")]
        public IActionResult PostComment(int id, Comment comnment)
        {

            return Ok();
        }
    }
} 