﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using task_journal.Models;
using task_journal.Models.Contexts;

namespace task_journal.Controllers
{
    [Route("api/[controller]")]
    [ApiController]
    public class OptionsController : ControllerBase
    {
        ExecutorsContext executorsContext;
        PlaceContext placeContext;
        public OptionsController(ExecutorsContext executorsCont, PlaceContext placeCont)
        {
            executorsContext = executorsCont;
            placeContext = placeCont;
        }
        [Route("executors")]
        [HttpGet]
        public IEnumerable<Executor> GetExecutors()
        {
            List<Executor> executors = executorsContext.Executors.ToList();
            return executors;
        }
        [Route("places")]
        [HttpGet]
        public IEnumerable<Place> GetPlaces()
        {
            IEnumerable<Place> testlist = placeContext.Places;
            return testlist;
        }

    }
}