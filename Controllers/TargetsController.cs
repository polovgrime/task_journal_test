﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.IO;
using System.Security.Claims;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using task_journal.Models;
using task_journal.Models.Contexts;
using task_journal.Models.ViewModels;

namespace task_journal.Controllers
{
   [Authorize]
    public class TargetsController : Controller
    {
        
        
        ExecutorsContext executorsContext;
        TargetsContext taskContext;
        PlaceContext placeContext;
        CommentContext commentContext;
        List<Target> targets;

        public TargetsController(TargetsContext targetsContext, ExecutorsContext eContext, PlaceContext pContext, CommentContext cc)
        {
            taskContext = targetsContext;
            executorsContext = eContext;
            placeContext = pContext;
            commentContext = cc;
            targets = taskContext.Tasks.ToList();
        }

        [Route("task/alltasks/remove/{taskid}/{commentid}")]
        public IActionResult TaskPage(int taskid, int commentid)
        {
            
            Comment com = commentContext.Comments.FirstOrDefault(x => x.Id == commentid);
            com.IsHidden = 1;
            if (!(com.Author == (User.Claims.First(e => e.Type == ClaimTypes.Name).Value))
                            && !User.IsInRole(Roles.Chief) && !User.IsInRole(Roles.Executor))
                return View($"{com.Id}"); if (com != null)
                commentContext.Comments.Update(com);
            commentContext.SaveChanges();
            return Redirect("/task/" + taskid);
        }
        [Route("alltasks")]
        public IActionResult AllTasks()
        {
            //ViewBag.
            return View();
        }

        [Route("task/{id}/new_comment")]
        [HttpPost]
        public IActionResult NewComment(TaskModel model)
        {
            Comment comment = new Comment { Text = model.NewestComment, Author = User.Claims.First(e => e.Type == ClaimTypes.Name).Value,
                TaskId = model.Id, Date = DateTime.Now, IsHidden = 0};
           
            commentContext.Comments.Add(comment);
            commentContext.SaveChanges();
            return (Redirect("/task/"+ model.Id));
        }

        [Route("task/{id}")]
        public IActionResult TaskPage(int id)
        {
            var Target = GetTask(id);
            ViewBag.Places = placeContext.Places.ToList();
            ViewBag.Comments = commentContext.Comments.Where(comment => comment.TaskId == id && comment.IsHidden == 0).OrderByDescending(element => element.Id).ToList();

            return View( new TaskModel{ Id = id, Place = Target.Place, Text = Target.Text, Theme = Target.Theme, Executor = Target.Executor, Author = Target.Author} );
        }
        [Route("task/{id}")]
        [HttpPost]
        public IActionResult TaskPage(TaskModel updated)
        {
            if (!ModelState.IsValid)
            {
                foreach(var val in ModelState.Values)
                    foreach(var error in val.Errors)
                        updated.Text += "\n" + error.ErrorMessage + "\n" + error.Exception;
                return View(updated);
            
            }
            if (!taskContext.Tasks.Any(e =>  e.Id == updated.Id))
                return NotFound("There's no such Id");
            var target = taskContext.Tasks.First(e => e.Id == updated.Id);
            if (!(target.Author == (User.Claims.First(e => e.Type == ClaimTypes.Name).Value))
                && !User.IsInRole(Roles.Chief) && !User.IsInRole(Roles.Executor))
                return View($"{updated.Id}");
            target.Place = updated.Place;
            target.Text = updated.Text;
            target.Theme = updated.Theme;
            updated.Executor = target.Executor;
            updated.Author = target.Author;
            try
            {
                taskContext.Update(target);
                taskContext.SaveChanges();
            } catch (Exception e)
            {
                return BadRequest("Unexpected error: " + e.Message);
            }

            ViewBag.Places = placeContext.Places.ToList();
            ViewBag.Comments = commentContext.Comments.Where(comment => comment.TaskId == updated.Id).ToList();
            ViewBag.Message = "Изменения сохранены!";
            return View(updated);
        }
        [Route("api/targets/{id}")]
        [HttpPut]
        public IActionResult Put (int id)
        {
            if (id <= 0)
                return BadRequest("Неправильный ид");
            if (!taskContext.Tasks.Any(e => e.Id == id)) 
                return BadRequest("Нет такого задания");
            
            Target tg = taskContext.Tasks.FirstOrDefault(e => e.Id == id);
            if (!(tg.Author == (User.Claims.First(e => e.Type == ClaimTypes.Name).Value))
                && !User.IsInRole(Roles.Chief) && !User.IsInRole(Roles.Executor))
                return BadRequest("Нет прав");
            tg.IsClosed = tg.IsClosed >= 1 ? 0 : 1;
            taskContext.Tasks.Update(tg);
            taskContext.SaveChanges();
            return Ok();
        }

        [Route("api/targets/task_id/{id}")]
        [HttpGet]
        public Target GetTask(int id)
        {
            if (!taskContext.Tasks.Any(e => e.Id == id))
                return new Target();
            return taskContext.Tasks.FirstOrDefault(e => e.Id == id);
        }

        [Route("api/targets/{sorting}/{direction}")]
        [HttpGet]
        public IEnumerable<Target> GetSorted(string sorting, string direction)
        {

            SortToken token = GetToken(sorting, direction);
            Sorter sorter = new Sorter(targets, token);
            targets = sorter.GetSortedTable().ToList();
            return targets;
        }

        SortToken GetToken(string sorting, string dir)
        {
            SortStyle style = SortStyle.ByDate;
            SortDirection direction = SortDirection.Down;
            SortToken sortToken;
            switch (sorting)
            {
                case "place":
                    style = SortStyle.ByPlace;
                break;
                case "date":
                    style = SortStyle.ByDate;
                break;
                case "author":
                    style = SortStyle.ByAuthor;
                break;
                case "executor":
                    style = SortStyle.ByExecutor;
                break;
                case "id":
                    style = SortStyle.ById;
                break;
                case "theme":
                    style = SortStyle.ByTheme;
                break;
                case "condition":
                    style = SortStyle.ByCondition;
                break;
            }
            switch (dir)
            {
                case "down":
                    direction = SortDirection.Down;
                break;
                case "up":
                    direction = SortDirection.Up;
                break;
            }
            sortToken = new SortToken(style, direction);
            return sortToken;

        }
    }

}
