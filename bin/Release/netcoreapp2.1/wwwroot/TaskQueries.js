﻿function GetTasks() {
    reset();
    $.ajax({
        url: "/api/targets",
        type: 'GET',
        contentType: "application/json",
        success: function (users) {
            alert("called that");
            var rows = "";
            $.each(users, function (index, user) {
                // добавляем полученные элементы в таблицу
                rows += row(user);
            });
            var lable = document.getElementById("testLabel");
               lable.innerHTML = "Без параметров";
            $("table tbody").append(rows);
        },
        error: function () {
        }
    });
};
var row = function (task) {
    var taskCondition;
    if (task.isClosed >= 1)
        taskCondition = "Закрыта";
    else
        taskCondition = "открыта"
    return "<tr data-rowid='" + task.id + "'><td>" + task.id + "</td> <td>" + task.date + "</td> <td>" + 
        task.place + "</td> <td>" + task.theme + "</td> <td>" + task.text + "</td> <td>" +
        task.executor + "</td> <td>" + task.author + "</td> <th>" + taskCondition + "</td> </tr > ";
}


function CreateTask(target) {
    $.ajax({
        url: "/api/targets",
        contentType: "application/json",
        method: "POST",
        data: JSON.stringify({
            theme: target.theme,
            text: target.text,
            executor: target.executor,
            author: target.author,
            place: target.place
        }),
        error: function () {
            console.log("oishbka");

        },
        success: function () {
            
            GetTasks();
        }
    })
}
function reset() {
    var form = document.forms["newTaskForm"];
    form.reset();
    form.elements["id"].value = 0;
}

function GetExecutors(selectObject) {
    var executors;
    $.ajax({
        url: "/api/targets/executors",
        type: "GET",
        success: function (data) {
            executors = data;
            for (var i = 0; i < executors.length; i++) {
                console.log(executors[i]);
                var executorName = document.createElement("option");
                executorName.appendChild(document.createTextNode(executors[i].name));
                executorName.setAttribute("value", executors[i].name);
                selectObject.appendChild(executorName);
            }
        }
    });
}

function GetPlaces(placeSelection) {
    $.ajax({
        url: "api/targets/places",
        type: "GET",
        success: function (places) {
            for (var i = 0; i < places.length; i++) {
                console.log(places[i]);
                var placeName = document.createElement("option");
                placeName.appendChild(document.createTextNode(places[i].name));
                placeName.setAttribute("value", places[i].name);
                placeSelection.appendChild(placeName);
            }
        }
    });
}

function GetTasks(sorting, direction) {
  //  reset();
    $.ajax({
        url: "/api/targets/" + sorting + "/" + direction,
        type: "GET",
        contentType: "application/json",
        success: function (users) {
            var rows = "";
            $.each(users, function (index, user) {
                // добавляем полученные элементы в таблицу
                rows += row(user);
            });
            var lable = document.getElementById("testLabel");
            lable.innerHTML = "Множественные  параметры";

            $("table tbody").append(rows);
        },
        error: function () {
            alert("");
        }
    });
};

